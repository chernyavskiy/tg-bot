package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	u "./util"
)

var baseURLs = []string{
	"https://map.nskgortrans.ru/markers.php", //?r=3-0002-W-2%257C",
	//"https://map.nskgortrans.ru/trasses.php", //?r=3-0002-W-2%257C",
}

/**
url create rule - <baseUrl>?r=<vType>-<prefixRoute>-W-<route>%7C(<vType>-<prefixRoute>-W-<route>%7C....)
	"https://map.nskgortrans.ru/trasses.php?r=3-0002-W-2%7C",
	prefixRoute for trams - 4 digits with leading zeros, rest - just route
*/

var vTypes = map[string]string{
	"transport":  "0", // Transport
	"bus":        "1", // Bus
	"trolleybus": "2", // Trolleybus
	"tram":       "3", // Tram
	"minibus":    "8", // Minibus
}

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("Usage <name> <type> <route>\n")
		os.Exit(0)
	}
	vType, route := os.Args[1], os.Args[2]
	start := time.Now()
	ch := make(chan string)
	for _, baseURL := range baseURLs {
		go fetch(baseURL, vTypes[vType], route, ch)
	}

	for range baseURLs {
		data := <-ch
		if u.IsMarkers(data) {
			vehicles, err := u.MarkersDecode(data)
			if err != nil {
				fmt.Printf("Json decode error: %s", err)
				fmt.Printf("%s", data)
			}
			fmt.Printf("%s\n", u.TemplatedRoute(vehicles))
		} else {
			trasses, err := u.TrassesDecode(data)
			if err != nil {
				fmt.Printf("Json decode error: %s", err)
				fmt.Printf("%s", data)
			}
			fmt.Printf("%v", trasses)
		}
	}
	fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())
}

func fetch(baseUrl, vType, route string, ch chan<- string) {
	var routePrefix string
	/*
		if vType == "3" {
			routePrefix = fmt.Sprintf("%04s", route)
		} else {
			routePrefix = route
		}
		ioutePrefix = route
	*/
	switch vType {
	case "3":
		routePrefix = fmt.Sprintf("%04s", route)
	default:
		routePrefix = route
	}
	url := fmt.Sprintf("%s?r=%s-%s-W-%s%%7C", baseUrl, vType, routePrefix, route)
	fmt.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("error : %v", err)
		ch <- fmt.Sprint(err)
		return
	}
	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		ch <- fmt.Sprintf("while reading %s: %v", url, err)
		return
	}
	ch <- string(b)
}
