package util

import (
	"encoding/json"
)

// IsMarkers services for markers data detection
func IsMarkers(d string) bool {
	start := d[2:9]
	return start == "markers"
}

//MarkersDecode convert json to Markers type
func MarkersDecode(ms string) (*Vehicles, error) {
	var result Vehicles
	if err := json.Unmarshal([]byte(ms), &result); err != nil {
		return nil, err
	}
	return &result, nil
}

//TrassesDecode convert json to Trasses
func TrassesDecode(ts string) (*Trasses, error) {
	var result Trasses
	if err := json.Unmarshal([]byte(ts), &result); err != nil {
		return nil, err
	}
	return &result, nil
}

/*
//GetClosestStop calculate closest stop for marker
func GetClosestStop(marker *Marker, trasses *Trasses) (*Point, error) {

}
*/
