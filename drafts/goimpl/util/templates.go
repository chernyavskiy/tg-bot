package util

import (
	"bytes"
	"log"
	s "strings"
	"text/template"
)

const markersTmpl = `
{{range .Markers}}
Schedule: {{.Graph}}
Speed: {{.Speed}}
Direction: {{.Direction}}
Timetable:
{{.Rasp | formatTT}}
{{end}}
`

/*
func getDirection(str string) string {
	var direction string
	if str == "A" {
		direction = "На Маркса"
	} else {
		direction = "На ЧС"
	}
	return direction
}
*/

func formatTT(str string) string {
	return s.Join(s.Split(s.Replace(str, "+", " - ", -1), "|"), "\n")
}

//TemplatedRoute ...
func TemplatedRoute(markers *Vehicles) string {
	var report = template.Must(template.New("markers").Funcs(
		template.FuncMap{
			//"getDirection": getDirection,
			"formatTT": formatTT,
		}).Parse(markersTmpl))
	var tpl bytes.Buffer
	if err := report.Execute(&tpl, &markers); err != nil {
		log.Fatal(err)
	}
	return tpl.String()
}
