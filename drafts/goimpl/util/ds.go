package util

import (
	"fmt"
)

// Marker implement data struct for vehicle unit on map
type Marker struct {
	Title        string  `json:"title,omitempty"`
	Marsh        string  `json:"marsh,omitempty"`
	Graph        string  `json:"graph,omitempty"`
	Direction    string  `json:"direction,omitempty"`
	Lat          float32 `json:"lat,string,omitempty"`
	Lng          float32 `json:"lng,string,omitempty"`
	TimeNav      string  `json:"time_nav,omitempty"`
	IDTypeTr     string  `json:"id_typetr,omitempty"`
	Azimuth      int     `json:"azimuth,string,omitempty"`
	Rasp         string  `json:"rasp,omitempty"`
	Speed        int     `json:"speed,string,omitempty"`
	SegmentOrder string  `json:"segment_order,omitempty"`
	Ramp         int     `json:"ramp,string,omitempty"`
}

func (m Marker) String() string {
	return fmt.Sprintf("%s, %d, %s\n%s\n", m.Marsh, m.Speed, m.Direction, m.Rasp)
}

//Vehicles implement data structure for vehicles on the map
type Vehicles struct {
	Markers []Marker `json:"markers"`
}

//Point implement route point, if it has name - it's vehicle stop
type Point struct {
	ID     string  `json:"id,omitempty"`
	Lat    float32 `json:"lat,string"`
	Lng    float32 `json:"lng,string"`
	Name   string  `json:"n,omitempty"`
	Length string  `json:"len,omitempty"`
}

func (p Point) String() string {
	return fmt.Sprintf("%s, %2.18f, %2.18f\n", p.Name, p.Lat, p.Lng)
}

//Route implement route dataset
type Route struct {
	PC     string  `json:"pc,omitempty"`
	Marsh  string  `json:"marsh"`
	Points []Point `json:"u"`
}

//Trass implement list of routes
type Trass struct {
	Routes []Route `json:"r"`
}

//Trasses implement trasses dataset
type Trasses struct {
	Trasses []Trass `json:"trasses"`
}
