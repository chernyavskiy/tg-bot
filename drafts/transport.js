const https = require('https');

( async () => {
  const url_markers = 'https://map.nskgortrans.ru/markers.php';
  const url_trasses = 'https://map.nskgortrans.ru/trasses.php';
  const queryString = (type, number) => `?r=${type}-${
    type === 3
      ? `000${number}`.slice(-4)
      : `${number}`
  }-W-${number}%257C`;

  let getData = (base_url, type, number) => new Promise((resolve, reject) => {
    let url = `${base_url}${queryString(type, number)}`;
    https.get(url, (res, err) => {
      if (err) {
        reject(err);
      };

      let data = '';
      res.on('data', chunk => data += chunk);
      res.on('end', () => {
        resolve(data);
      });
    });
  });

  let markersRaw;
  try {
    markersRaw = await getData(url_markers, 3, 2);
  } catch(err) {
    console.log(err);
  }
  const { markers } = JSON.parse(markersRaw);

  const getStopInfo = name => {
    let hasInRasp = markers.filter(item => item.rasp.includes(name));
    let to4S = hasInRasp.filter(item => item.direction === 'B').reduce( (a,c) => {
      let data = c.rasp.split('|').filter( str => str.includes(name));
      return `${a}\n${data.join('\n')}`;
    }, 'to 4S:');
    let from4S = hasInRasp.filter(item => item.direction === 'A').reduce( (a,c) => {
      let data = c.rasp.split('|').filter( str => str.includes(name));
      return `${a}\n${data.join('\n')}`;
    }, 'from 4S:');

    return `${from4S}\n\n${to4S}`;
  };

  let trassesRaw;
  try {
    trassesRaw = await getData(url_trasses, 3, 2);
  } catch(err) {
    console.log(err);
  }

  const { trasses } = JSON.parse(trassesRaw);
  let { r } = trasses[0];
  let marsh = r.find(item => item.marsh.includes('2'));
  let { u } = marsh;
  const stops = u.filter(item => item.id);
  let deg2rad = deg => Number(deg) * Math.PI / 180;
  const EARTH_RADIUS = 6371000;
  const getPolar = (a,b) => {
    const latA = deg2rad(a.lat);
    const latB = deg2rad(b.lat);
    const lngA = deg2rad(a.lng);
    const lngB = deg2rad(b.lng);
    const dist = 2 * EARTH_RADIUS * Math.asin(
      Math.sqrt(
        Math.sin(
          (latA - latB)/2
        )**2 + Math.cos(latA) * Math.cos(latB) *
        (Math.sin(
          (lngA - lngB)/2
        )**2)
      )
    );

    return dist;
  };

  const closestMap = (stops, cars) => cars.map( car => {
    let nearest = stops.sort( (a,b) => {
      let polarA = getPolar(a,car);
      let polarB = getPolar(b,car);
      return polarA - polarB;
    })[0];
    car.nearest = nearest;
    car.nearest.dist = getPolar(nearest, car);
    return car;
  });

  let x = closestMap(stops, markers);
  //console.log(x);
  const getEndStopTime = vehicle => {
    let rasp = vehicle.rasp.split('|').find( stop => stop.includes('Чистая Слобода') || stop.includes('Площадь К.Маркса'));
    return (
      rasp
        ? rasp.split('+')[0]
      : ' - '
    );
  };
  const infoTmpl = vehicle => `График №${vehicle.graph}, Скорость - ${vehicle.speed} км/ч\nМестонахождение - ${vehicle.nearest.n}\nНаправление - ${
    vehicle.direction === 'B' 
      ? 'на Слободу'
      : 'на Маркса'
  }\nПрибытие на конечную - ${getEndStopTime(vehicle)}\n`;
  x.forEach( vehicle => console.log(infoTmpl(vehicle)));

})();
