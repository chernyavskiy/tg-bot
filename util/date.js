const MS4DAY = 1000 * 60 * 60 * 24;
/**
 * getLatency() return latency in ms 
 * @param {String} time
 * @return {Number} latency
*/
const getLatency = time => {
  let [h,m] = time.split(/[.:]/).map(Number);
  let [eh, em] = (() => {
    let time = new Date();
    return [time.getHours(), time.getMinutes()]
  })();
  let addM = 60 + m - em;
  let addH = eh >= h && em > m ? 23 - eh + h : h - eh - 1;
  let latency = (3600 * addH + 60 * addM) * 1000;
  return latency;
}

/**
 * getDaysGap() return gap between two date
 * @param {String} start - start date, format - DD.MM
 * @param {String} end - end date, format - DD.MM
 * @return {Number} gap
*/
const getDaysGap = (start, end) => {
  let split = x => x.split(/[.]/).map( (v,i) => i ? Number(v) - 1 : Number(v));
  let [d1,m1] = split(start);
  let [d2,m2] = split(end);
  let year = new Date().getFullYear();
  let currentYearGap = new Date(year,m2,d2) - new Date(year,m1,d1);
  let period = currentYearGap >= 0 ? 
    currentYearGap :
    new Date(year + 1,m2,d2) - new Date(year,m1,d1);
  return period / MS4DAY;
}

module.exports = {
  getLatency,
  getDaysGap
};
