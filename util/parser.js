const cheerio = require('cheerio');
const https = require('https');

const { url, user, password } = require('../config.json');

const options = {
  auth: `${user}:${password}`
}

/**
 * getData() return array of birthdays objects 
 * @param {String} department code
 * @return {Promise} Promise object represents the array of birthdays objects
*/
const getData = (code) => new Promise( (resolve, reject) => {
  let urlByDepartment = `${url}?department=${code}`;
  const req = https.get(urlByDepartment, options, res => {
    if (res.statusCode !== 200) {
      reject(new Error(`Got ${res.statusCode} response code`));
    }
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('end', () => {
      //handle data
      const $ = cheerio.load(data);
      const birthdays = [];
      $('.bx-user-info-big').each( (i,el) => {
        let date = $('.bx-user-subtitle', el).text().trim();
        let person = $('.bx-user-name', el).text().trim();
        let post = $('.bx-user-post', el).text().trim();
        birthdays.push({
          date,
          person,
          post
        });
      });
      resolve(birthdays);
    });
  });
});


/**
 * getWeather() return weather data from ngs.ru
 * @return {Promise} Promise object represents data object
 */
const getWeather = () => new Promise( (resolve, reject) => {
  const url = 'https://pogoda.ngs.ru';
  const req = https.get(url, res => {
    let data = '';
    res.on('data', chunk => data += chunk);
    res.on('end', () => {
      const $ = cheerio.load(data);
      const weather = {};
      let target = $('.today-panel__info__main__item');
      let temperature = $('.today-panel__temperature', target);
      weather.tempFact = $('.value', temperature).text().trim();
      weather.feelsLike = $('.value-feels_like', temperature).text().trim()
        .split(/[\s]/).filter(str => str).join(' ');
      weather.descr = $('.value-description', temperature).text().trim();
      let extra = $('dl', target);
      weather.extra = [];
      extra.each( (i,el) => {
        if (i < 5) {
          let name = $('dd',el).text().trim();
          let value = $('dt',el).text().split(/[\s]/).filter(str => str).join(' ');
          weather.extra.push({name,value});
        }
      });
      resolve(weather);
    });
  });
});

module.exports = { getData, getWeather }
