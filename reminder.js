const TelegramBot = require('node-telegram-bot-api');
const { getData, getWeather } = require('./util/parser');
const { getLatency, getDaysGap } = require('./util/date');
const { departments,
        token,
        broadcasting,
        interval,
        observablePeriod,
        alertTime } = require('./config');


const bot = new TelegramBot(token, { polling: true });

const notes = [];
const out_tmpl = item => `${item.date.slice(0,5)} - ${item.person}, ${item.post}`;
const broadcast_tmpl = item => `${item.gap} days left before ${item.person} birthday`;

const startDate = new Date();

const countdown = async () => {
  const now = new Date();
  const start = `${now.getDate()}.${now.getMonth() + 1 }`;
  const result = await Promise.all(
    departments.nmic.concat(departments.genom).map(getData)
  );
  const data = result.flat();
  data.forEach( rec => {
    rec.gap = getDaysGap(start, rec.date);
  });
  data.sort( (a,b) => a.gap - b.gap );

  const nearest = data.filter( rec => rec.gap < observablePeriod );
  if (nearest.length) {
    bot.sendMessage(broadcasting, nearest.map(broadcast_tmpl).join('\n'));
  }
};

const setCron = () => {
  const cron = setInterval(countdown, interval);
};

setTimeout( () => {
  countdown();
  setCron();
}, getLatency(alertTime));

bot.onText(/all/, async (msg, match) => {
  const chatId = msg.chat.id;
  const result = await Promise.all(
    departments.nmic.concat(departments.genom).map(getData)
  );
  const data = result.flat();
  let start = `${new Date().getDate()}.${new Date().getMonth() + 1}`;
  data.forEach( rec => {
    rec.gap = getDaysGap(start, rec.date);
  });
  data.sort( (a,b) => a.gap - b.gap );
  bot.sendMessage(chatId, data.map(out_tmpl).join('\n'));
});

bot.onText(/next/, async (msg, match) => {
  const chatId = msg.chat.id;
  const result = await Promise.all(
    departments.nmic.concat(departments.genom).map(getData)
  );
  const data = result.flat();
  let next = data.filter( item => {
    let [day, month] = item.date.split('.').map( (v,i) => i ? Number(v) - 1 : Number(v));
    const [ dayNow, monthNow ] = (() => {
      let now = new Date();
      return [ now.getDate(), now.getMonth() ];
    })();
    return month === monthNow && day > dayNow;
  });
  bot.sendMessage(chatId, next.length ? next.map(out_tmpl).join('\n') : 'No birthdays this month' )
});

bot.onText(/genom/, async (msg, match) => {
  const chatId = msg.chat.id;
  const result = await Promise.all(
    departments.genom.map(getData)
  );
  const data = result.flat();
  let start = `${new Date().getDate()}.${new Date().getMonth() + 1}`;
  data.forEach( rec => {
    rec.gap = getDaysGap(start, rec.date);
  });
  data.sort( (a,b) => a.gap - b.gap );
  bot.sendMessage(chatId, data.map(out_tmpl).join('\n'));
});

bot.onText(/nmic/, async (msg, match) => {
  const chatId = msg.chat.id;
  const result = await Promise.all(
    departments.nmic.map(getData)
  );
  const data = result.flat();
  let start = `${new Date().getDate()}.${new Date().getMonth() + 1}`;
  data.forEach( rec => {
    rec.gap = getDaysGap(start, rec.date);
  });
  data.sort( (a,b) => a.gap - b.gap );
  bot.sendMessage(chatId, data.map(out_tmpl).join('\n'));
});

bot.onText(/weather/, async (msg, match) => {
  const chatId = msg.chat.id;
  let data = await getWeather();
  let message = `Температура: ${data.tempFact}\n${data.feelsLike}\n${data.descr}\n${data.extra.map(i => `${i.name} ${i.value}`).join('\n')}`;

  bot.sendMessage(chatId, message);
});

bot.onText(/remind (.+) at (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  const text = match[1];
  const time = match[2];

  notes.push({
    uid: chatId,
    time,
    text
  });

  bot.sendMessage(chatId, 'I will remind you if I would be still alive ;)');
});

setInterval( () => {
  const now = new Date();
  if (notes.length) {
    let timeOutNotes = notes.filter( n => {
      let [hours, minutes] = n.time.split(/[.:]/).map(Number);
      return hours === now.getHours() && minutes === now.getMinutes();
    });
    if (timeOutNotes.length) {
      timeOutNotes.forEach( note => {
        bot.sendMessage(note.uid, note.text);
        notes.splice(notes.indexOf(note),1);
      });
    }
  }
}, 1000);
